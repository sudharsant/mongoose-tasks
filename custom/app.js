var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var userSchema = new Schema({
  name: String,
  username: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  admin: Boolean,
  location: String
});

userSchema.methods.dudify = function(names) {
  this.names = names+'-dude';
  return this.names;
};

userSchema.methods.find = function(cb) {
  return this.model('User', userSchema).find({type: this.type }, cb);
}

var User = mongoose.model('User', userSchema);

module.exports = User;

