var User = require('./app');

var express = require('express');

var app = express();

var newUser = new User({
  name: 'Peter Thiel',
  username: 'entrepreneur',
  password: 'onthego',
  admin: true
})

newUser.save(function(err) {
  if(err) throw err;

  console.log('User created');
});

User.find({ username: 'entrepreneur' }, function(err, user) {
  if(err) throw err

  console.log(user);
});

app.get('/', function(req,res) {
  console.log('something')
  res.end('Done:'+newUser.password)
})

app.listen(8000)