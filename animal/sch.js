
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var express = require('express');

var app = express();

var animalSchema = new Schema({ name: String, type: String });

animalSchema.methods.findSimilarTypes = function(cb) {
  return this.model('Animal').find({ type: this.type }, cb);
};

var Animal = mongoose.model('Animal', animalSchema);
var dog = new Animal({ type: 'dog' });

dog.findSimilarTypes(function(err, dogs) {
  console.log(dogs);
});

app.get('/', function(req,res) {
  console.log('something')
  res.end('Done:')
});

app.listen(8000);